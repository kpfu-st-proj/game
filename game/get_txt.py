def get_txt(filepath):
    with open(filepath) as f:
        return f.read()
