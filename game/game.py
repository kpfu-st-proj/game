import random
import socket
import threading
import time
import os
from .user import User
from .get_txt import get_txt


class GameServer:
    def __init__(self, instructions, host='127.0.0.1', port=5000, max_players_cnt=10, duration=60):
        self.host = host
        self.port = port
        self.duration = duration
        self.ready_players_count = 0
        self.max_players_cnt = max_players_cnt
        self.players = []
        self.is_game_started = False
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((self.host, self.port))
        self.instructions = instructions
        self.gameThread = threading.Thread(target=self.check_all_players_on_ready).start()
        self.current_item = 0

    def check_all_players_on_ready(self):
        while True:
            while True:
                if self.ready_players_count == len(self.players) and len(self.players) > 1:
                    break

            for p in self.players:
                p.in_game = True
                p.connection.send('Start game!'.encode('utf-8'))

            self.is_game_started = True
            self.game_loop()

    def listen(self):
        self.sock.listen(self.max_players_cnt)
        while True:
            connection, address = self.sock.accept()
            player = User(
                nickname=len(self.players) + 1,
                connection=connection,
            )
            threading.Thread(target=self.listen_client, args=(player,)).start()
            self.players.append(player)

            for p in self.players:
                p.connection.send(f'{self.show_all_players()}'.encode('utf-8'))

    def listen_client(self, player):
        size = 1024
        message = self.instructions
        message += f'Your nickname {player.nickname}'
        player.connection.send(message.encode(encoding='utf-8'))

        while True:
            try:
                data = player.connection.recv(size).decode(encoding='utf-8').strip().lower()
                if not player.in_game:
                    if 'nick:' in data:
                        data = data.split(':')
                        player.nickname = data[1]
                        message = f'Successfully changed on {player.nickname}'

                    elif data == 'changestatus':
                        player.is_ready = not player.is_ready
                        if player.is_ready:
                            self.ready_players_count += 1
                        else:
                            self.ready_players_count -= 1

                        message = f"Your ready status: {player.is_ready}!"
                        if self.is_game_started:
                            message += '\nThe game is already running. Please wait.'

                    elif data == 'help':
                        message = self.instructions

                    elif data == 'disconnect':
                        self.players.remove(player)
                        player.connection.close()
                        return

                    elif data == 'showallplayers':
                        message = self.show_all_players()

                    else:
                        message = 'Invalid command!\nEnter <help> for instructions'

                    player.connection.send(message.encode(encoding='utf-8'))
                elif self.current_item == 1:
                    player.points += 1
                    self.current_item = ''
                elif self.current_item == 0:
                    player.points -= 1
                    self.current_item = ''

            except Exception as e:
                print(e)
                return

    def game_loop(self):
        fish_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'txt/fish.txt')
        trash_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'txt/trash.txt')
        fish = get_txt(fish_path)
        trash = get_txt(trash_path)
        start = time.time()
        while time.time() - start < self.duration:
            rand_timing = random.randint(2, 4)
            start_pause = time.time()
            container = (trash, fish)
            item = random.randint(0, 1)
            time.sleep(rand_timing)
            self.current_item = item
            for p in self.players:
                if p.in_game:
                    p.connection.send(f'{p.points}\n\n{container[item]}'.encode('utf-8'))

        self.end_game()

    def end_game(self):
        results = ''
        in_game_players = [p for p in self.players if p.in_game]
        sorted_players = sorted(in_game_players, key=lambda p: p.points, reverse=True)
        for p in sorted_players:
            results += f'{sorted_players.index(p) + 1}. {p.nickname}: {p.points}\n'

        for p in sorted_players:
            if p.in_game:
                p.connection.send(results.encode('utf-8'))
                p.is_ready = False
                p.points = 0
                p.in_game = False

        self.ready_players_count = 0
        self.is_game_started = False

    def show_all_players(self):
        players_with_status = ''
        for player in self.players:
            players_with_status += f'{player.nickname}. Is ready: {player.is_ready}. In game: {player.in_game}\n'
        return players_with_status
