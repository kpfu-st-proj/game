class User:
    def __init__(self, nickname, connection):
        self.connection = connection
        self.nickname = nickname
        self.is_ready = False
        self.points = 0
        self.in_game = False

    def add_one_point(self):
        self.points += 1
        return self.points

    def subtract_one_point(self):
        self.points -= 1
        return self.points
