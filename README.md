### How to play?
The player is a cat. Either fish or its scraps appear in the terminal. 
If there is a fish in the terminal and the player manages to press the enter key first, 
then he gets +1 point, if fish scraps, then -1 point. 
The winner is the one who gets the most points in a given time.

### Description of the protocol specification.

> 1. All connected players enter the idle room where they can see how many players are in the room and how many players are ready to play.
> 2. Next, the server waits until all players change their status to ready from all connected (changestatus).
> 3. Every time someone connects, the information in the terminal is updated for everyone.
> 4. When all players are ready, the game begins.
> 5. The server sends a sequence with an arbitrary interval - the image and the number of points for each player.
> 6. The first person to press the button will receive or deduct points. A new line appears - a picture and the number of points.
> 7. Items (5, 6) are repeated until the end of the game.
> 8. All players get the results of all players.
> 9. Next, if the player enters "disconnect", the connection is disconnected. If not, it waits on the server and can play again.
# All commands can be viewed in the file 'txt / instructions.txt' 
