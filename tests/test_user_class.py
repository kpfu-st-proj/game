def test_user_add_point_method(create_test_user):
    u = create_test_user
    u.add_one_point()
    assert u.points == 1


def test_user_subtract_point_method(create_test_user):
    u = create_test_user
    u.subtract_one_point()
    assert u.points == -1
