import threading
from game.game import GameServer


def test_listen_method(create_test_user):
    def server(game):
        game.listen()

    def client():
        u = create_test_user
        with u.connection:
            u.connection.connect(('127.0.0.1', 5000))
            while True:
                pass

    game = GameServer(instructions='qwwrewre')
    threading.Thread(target=server, args=(game, )).start()
    threading.Thread(target=client).start()

    assert len(game.players) == 1
